package com.example.crypto_diary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CryptoDiaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(CryptoDiaryApplication.class, args);
	}

}
