package com.example.crypto_diary.models;

public enum TransactionType {
    BUY,
    SELL
}
