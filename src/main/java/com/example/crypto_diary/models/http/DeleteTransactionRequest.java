package com.example.crypto_diary.models.http;

import com.example.crypto_diary.models.Transaction;

public class DeleteTransactionRequest {
    private Transaction transaction;
    private String groupName;

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
