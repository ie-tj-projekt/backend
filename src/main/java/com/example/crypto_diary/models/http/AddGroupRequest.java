package com.example.crypto_diary.models.http;

import com.example.crypto_diary.models.Transaction;

import java.util.Date;
import java.util.List;

public class AddGroupRequest {
    private String name;
    private List<Transaction> transactions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
