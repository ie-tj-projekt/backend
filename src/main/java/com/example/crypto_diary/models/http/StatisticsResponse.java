package com.example.crypto_diary.models.http;

public class StatisticsResponse {
    private int numberOfGroups;
    private int numberOfTransactions;
    private int numberOfBuy;
    private int numberOfSell;
    private float moneyInvested;
    private float moneyCashed;

    public StatisticsResponse(int numberOfGroups, int numberOfTransactions, int numberOfBuy, int numberOfSell, float moneyInvested, float moneyCashed) {
        this.numberOfGroups = numberOfGroups;
        this.numberOfTransactions = numberOfTransactions;
        this.numberOfBuy = numberOfBuy;
        this.numberOfSell = numberOfSell;
        this.moneyInvested = moneyInvested;
        this.moneyCashed = moneyCashed;
    }

    public int getNumberOfGroups() {
        return numberOfGroups;
    }

    public void setNumberOfGroups(int numberOfGroups) {
        this.numberOfGroups = numberOfGroups;
    }

    public int getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public void setNumberOfTransactions(int numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }

    public int getNumberOfBuy() {
        return numberOfBuy;
    }

    public void setNumberOfBuy(int numberOfBuy) {
        this.numberOfBuy = numberOfBuy;
    }

    public int getNumberOfSell() {
        return numberOfSell;
    }

    public void setNumberOfSell(int numberOfSell) {
        this.numberOfSell = numberOfSell;
    }

    public float getMoneyInvested() {
        return moneyInvested;
    }

    public void setMoneyInvested(float moneyInvested) {
        this.moneyInvested = moneyInvested;
    }

    public float getMoneyCashed() {
        return moneyCashed;
    }

    public void setMoneyCashed(float moneyCashed) {
        this.moneyCashed = moneyCashed;
    }
}
