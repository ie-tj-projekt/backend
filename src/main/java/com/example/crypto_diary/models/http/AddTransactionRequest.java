package com.example.crypto_diary.models.http;

import com.example.crypto_diary.models.Transaction;

public class AddTransactionRequest {
    private String groupName;
    private Transaction transaction;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
