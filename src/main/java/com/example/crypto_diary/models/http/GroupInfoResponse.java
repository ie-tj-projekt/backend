package com.example.crypto_diary.models.http;

public class GroupInfoResponse {
    private String symbol;
    private Float amount;

    public String getSymbol() {
        return symbol;
    }

    public void addAmount(Float amount) {
        this.amount += amount;
    }

    public void subAmount(Float amount) {
        this.amount -= amount;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }
}
