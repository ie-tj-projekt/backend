package com.example.crypto_diary.models;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Group {
    private String name;
    private Long creationDate;
    private List<Transaction> transactions;

    public Group(String name, Long creationDate, List<Transaction> transactions) {
        this.name = name;
        this.creationDate = Objects.requireNonNullElseGet(creationDate, () -> new Date().getTime());
        this.transactions = Objects.requireNonNullElseGet(transactions, LinkedList::new);
    }

    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
