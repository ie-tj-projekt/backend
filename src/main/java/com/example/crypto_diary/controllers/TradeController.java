package com.example.crypto_diary.controllers;

import com.example.crypto_diary.PdfGenerator;
import com.example.crypto_diary.models.Group;
import com.example.crypto_diary.models.Transaction;
import com.example.crypto_diary.models.TransactionType;
import com.example.crypto_diary.models.User;
import com.example.crypto_diary.models.http.*;
import com.example.crypto_diary.repositories.CoinRepository;
import com.example.crypto_diary.repositories.UserRepository;
import com.example.crypto_diary.security.jwt.JwtUtils;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class TradeController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    CoinRepository coinRepository;
    @Autowired
    JwtUtils jwtUtils;

    @RequestMapping(value = "/add_group", method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> addGroup(@RequestBody AddGroupRequest body,
                                      @RequestHeader (name="Authorization") String token) {

        token = token.split(" ")[1];
        Optional<User> u = userRepository.findByEmail(jwtUtils.getUserNameFromJwtToken(token));
        
        if (u.isPresent()) {
            User user = u.get();
            Group group = user.getGroups().stream()
                    .filter(t -> t.getName().equals(body.getName()))
                    .findAny()
                    .orElse(null);

            if (group == null) {
                List<Transaction> transactions = body.getTransactions();
                if (transactions != null) {
                    Iterator<Transaction> iterator = transactions.iterator();
                    while (iterator.hasNext()) {
                        Transaction t = iterator.next();
                        if (t.getType() == TransactionType.SELL) {
                            iterator.remove();
                        } else {
                            t.setDate(new Date().getTime());
                            t.setValue(t.getAmount() * t.getPrice());
                        }
                    }
                } else {
                    transactions = new LinkedList<>();
                }
                group = new Group(body.getName(), new Date().getTime(), transactions);
                user.addGroup(group);
                return ResponseEntity.ok(userRepository.save(user));
            } else {
                return ResponseEntity.badRequest().body(
                        new MessageResponse(String.format("Group '%s' already exists", group.getName()))
                );
            }
        }
        return ResponseEntity.badRequest().body(new MessageResponse("Unauthorized"));
    }

    @RequestMapping(value = "/add_transaction", method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> addTransaction(@RequestBody AddTransactionRequest body,
                                            @RequestHeader (name = "Authorization") String token) {

        token = token.split(" ")[1];
        Optional<User> u = userRepository.findByEmail(jwtUtils.getUserNameFromJwtToken(token));

        if (u.isPresent()) {
            User user = u.get();
            Group group = user.getGroups().stream()
                    .filter(t -> t.getName().equals(body.getGroupName()))
                    .findAny()
                    .orElse(null);

            if (group != null) {
                if (body.getTransaction().getType() == TransactionType.SELL) {
                    String symbol = body.getTransaction().getSymbol();
                    List<Transaction> list = group.getTransactions().stream()
                            .filter(t -> t.getSymbol().equals(symbol))
                            .collect(Collectors.toList());

                    float sum = 0;
                    for (Transaction t : list) {
                        if (t.getType() == TransactionType.BUY)
                            sum += t.getAmount();
                        else
                            sum -= t.getAmount();
                    }

                    if (body.getTransaction().getAmount() > sum)
                        return ResponseEntity.badRequest().body(String.format("You have not enough '%s' in group '%s", symbol, body.getGroupName()));
                }

                Transaction t = new Transaction();
                t.setType(body.getTransaction().getType());
                t.setSymbol(body.getTransaction().getSymbol());
                t.setAmount(body.getTransaction().getAmount());
                t.setPrice(body.getTransaction().getPrice());
                t.setValue(t.getAmount() * t.getPrice());
                t.setDate(new Date().getTime());
                group.addTransaction(t);
                return ResponseEntity.ok(userRepository.save(user));
            } else {
                return ResponseEntity.badRequest().body(
                        new MessageResponse(String.format("Group '%s' not found", body.getGroupName()))
                );
            }
        }
        return ResponseEntity.badRequest().body(new MessageResponse("Unauthorized"));
    }

    @RequestMapping(value = "/delete_group", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> deleteGroup(@RequestBody Group body,
                                         @RequestHeader (name = "Authorization") String token) {

        token = token.split(" ")[1];
        Optional<User> u = userRepository.findByEmail(jwtUtils.getUserNameFromJwtToken(token));

        if (u.isPresent()) {
            User user = u.get();
            Group group = user.getGroups().stream()
                    .filter(t -> t.getName().equals(body.getName()))
                    .findAny()
                    .orElse(null);

            if (group != null) {
                user.getGroups().remove(group);
                return ResponseEntity.ok(userRepository.save(user));
            } else {
                return ResponseEntity.badRequest().body(
                        new MessageResponse(String.format("Group '%s' not found", body.getName()))
                );
            }
        }
        return ResponseEntity.badRequest().body(new MessageResponse("Unauthorized"));
    }

    @RequestMapping(value = "/delete_transaction", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> deleteTransaction(@RequestBody DeleteTransactionRequest body,
                                               @RequestHeader (name = "Authorization") String token) {

        token = token.split(" ")[1];
        Optional<User> u = userRepository.findByEmail(jwtUtils.getUserNameFromJwtToken(token));

        if (u.isPresent()) {
            User user = u.get();
            Group group = user.getGroups().stream()
                    .filter(t -> t.getName().equals(body.getGroupName()))
                    .findAny()
                    .orElse(null);

            if (group != null) {
                Transaction transaction = group.getTransactions().stream()
                        .filter(t -> t.getDate().equals(body.getTransaction().getDate()))
                        .findAny()
                        .orElse(null);

                if (transaction != null) {
                    group.getTransactions().remove(transaction);
                    return ResponseEntity.ok(userRepository.save(user));
                } else {
                    return ResponseEntity.badRequest().body(
                            new MessageResponse("Transaction not found")
                    );
                }
            } else {
                return ResponseEntity.badRequest().body(String.format("Group '%s' not found", body.getGroupName()));
            }
        }
        return ResponseEntity.badRequest().body(new MessageResponse("Unauthorized"));
    }

    @RequestMapping(value = "/statistics", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getGroupInfo(@RequestHeader (name = "Authorization") String token) {

        token = token.split(" ")[1];
        Optional<User> u = userRepository.findByEmail(jwtUtils.getUserNameFromJwtToken(token));

        if (u.isPresent()) {
            User user = u.get();

            int numberOfGroups = user.getGroups().size();
            int numberOfTransactions = 0;
            int numberOfBuy = 0;
            int numberOfSell = 0;
            float moneyInvested = 0;
            float moneyCashed = 0;
            for (Group group : user.getGroups()) {
                List<Transaction> transactions = group.getTransactions();
                numberOfTransactions += transactions.size();
                for (Transaction t : transactions) {
                    if (t.getType() == TransactionType.BUY) {
                        numberOfBuy++;
                        moneyInvested += t.getValue();
                    } else {
                        numberOfSell++;
                        moneyCashed += t.getValue();
                    }
                }
            }

            StatisticsResponse response = new StatisticsResponse(
                    numberOfGroups,
                    numberOfTransactions,
                    numberOfBuy,
                    numberOfSell,
                    moneyInvested,
                    moneyCashed
            );

            return ResponseEntity.ok(response);
        }
        return ResponseEntity.badRequest().body(new MessageResponse("Unauthorized"));
    }

    @RequestMapping(value = "/report", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> report(@RequestParam List<String> body,
                                    @RequestHeader (name = "Authorization") String token) throws IOException {

        token = token.split(" ")[1];
        Optional<User> u = userRepository.findByEmail(jwtUtils.getUserNameFromJwtToken(token));

        if (u.isPresent()) {
            User user = u.get();
            try {
                String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat dateFormat = new SimpleDateFormat(TIMESTAMP_PATTERN);

                Document document = new Document();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                PdfWriter.getInstance(document, out);

                document.open();

                Font fontTitle = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
                Font font = FontFactory.getFont(FontFactory.COURIER, 11, BaseColor.BLACK);

                Paragraph title = new Paragraph("Crypto Report", fontTitle);
                Paragraph userName = new Paragraph(String.format("Name:   %s %s", user.getFirstName(), user.getLastName()), font);
                Paragraph email = new Paragraph(String.format("Email:  %s", user.getEmail()), font);
                Paragraph date = new Paragraph("Date:   " + dateFormat.format(new Date()), font);

                title.setAlignment(Element.ALIGN_CENTER);

                document.add(title);

                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));

                document.add(userName);
                document.add(email);
                document.add(date);

                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));

                for (Group group : user.getGroups()) {
                    if (body.contains(group.getName()) && group.getTransactions().size() != 0) {
                        Paragraph groupName = new Paragraph(group.getName(), font);
                        groupName.setAlignment(Element.ALIGN_CENTER);
                        document.add(groupName);
                        document.add(new Paragraph(" "));

                        PdfPTable transactionTable = new PdfPTable(6);
                        transactionTable.setWidths(new int[]{16, 4, 5, 8, 8, 8});
                        PdfGenerator.addTransactionsHeaders(transactionTable, font);
                        PdfGenerator.addTransactions(transactionTable, group.getTransactions(), font, dateFormat);
                        document.add(transactionTable);
                        document.add(new Paragraph(" "));

                        PdfPTable summaryTable = new PdfPTable(5);
                        summaryTable.setWidths(new int[]{5, 10, 10, 10, 10});
                        PdfGenerator.addSummaryHeaders(summaryTable, font);
                        try {
                            PdfGenerator.addSummaryData(summaryTable, group.getTransactions(), coinRepository, font);
                            document.add(summaryTable);
                            document.add(new Paragraph(" "));
                            document.add(new Paragraph(" "));
                        } catch (Exception exception) {
                            return ResponseEntity.badRequest().body(exception.getMessage());
                        }
                    }
                }

                document.close();

                return ResponseEntity
                        .ok()
                        .contentType(MediaType.APPLICATION_PDF)
                        .contentLength(out.size())
                        .body(out.toByteArray());

            } catch (DocumentException exception) {
                return ResponseEntity.badRequest().body(exception.getMessage());
            }
        }
        return ResponseEntity.badRequest().body("ERROR");
    }

}
