package com.example.crypto_diary;

import com.example.crypto_diary.models.Coin;
import com.example.crypto_diary.models.Transaction;
import com.example.crypto_diary.models.TransactionType;
import com.example.crypto_diary.repositories.CoinRepository;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PdfGenerator {

    public static void addTransactionsHeaders(PdfPTable table, Font font) {
        Stream.of("DATE", "TYPE", "SYM", "QTY", "PRICE", "VALUE")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setPhrase(new Phrase(columnTitle, font));
                    header.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    header.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
                    header.setFixedHeight(20);
                    table.addCell(header);
                });
    }

    public static void addTransactions(PdfPTable table, List<Transaction> list, Font font, SimpleDateFormat dateFormat) {
        for (Transaction t : list) {

            String amount = String.format("%,f", t.getAmount());
            if (amount.length() > 8)
                amount = amount.substring(0, 8).replaceAll(",", " ");

            String price = String.format("%,f", t.getPrice());
            if (price.length() > 8)
                price = price.substring(0, 8).replaceAll(",", " ");

            String value = String.format("%,f", t.getValue());
            if (value.length() > 8)
                value = value.substring(0, 8).replaceAll(",", " ");

            table.addCell(cell(dateFormat.format(t.getDate()), font));
            table.addCell(cell(t.getType().toString(), font));
            table.addCell(cell(t.getSymbol(), font));
            table.addCell(cell(amount, font));
            table.addCell(cell(price, font));
            table.addCell(cell(value, font));
        }
    }

    public static PdfPCell cell(String content, Font font) {
        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        return cell;
    }

    public static void addSummaryHeaders(PdfPTable table, Font font) {
        Stream.of("SYM", "BUY", "SELL", "DIFF", "VALUE")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setPhrase(new Phrase(columnTitle, font));
                    header.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    header.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
                    header.setFixedHeight(20);
                    table.addCell(header);
                });
    }

    public static void addSummaryData(PdfPTable table, List<Transaction> list, CoinRepository coinRepository, Font font) throws IOException, JSONException {
        List<String> coins = list.stream().map(Transaction::getSymbol).filter(distinctByKey(t -> t)).collect(Collectors.toList());
        float[] buy = new float[coins.size()];
        float[] sell = new float[coins.size()];
        for (Transaction t : list) {
            int i = coins.indexOf(t.getSymbol());
            if (t.getType() == TransactionType.BUY)
                buy[i] += t.getAmount();
            else
                sell[i] += t.getAmount();
        }

        for (int i = 0; i < coins.size(); i++) {
            float diff = buy[i] - sell[i];

            String buyString = String.format("%,f", buy[i]);
            if (buyString.length() > 12)
                buyString = buyString.substring(0, 12).replaceAll(",", " ");

            String sellString = String.format("%,f", sell[i]);
            if (sellString.length() > 12)
                sellString = sellString.substring(0, 12).replaceAll(",", " ");

            String diffString = String.format("%,f", diff);
            if (diffString.length() > 12)
                diffString = diffString.substring(0, 12).replaceAll(",", " ");

            table.addCell(cell(coins.get(i), font));
            table.addCell(cell(buyString, font));
            table.addCell(cell(sellString, font));
            table.addCell(cell(diffString, font));

            Optional<Coin> coin = coinRepository.findBySymbol(coins.get(i).toLowerCase(Locale.ROOT));
            if (coin.isPresent()) {
                Coin c = coin.get();
                URL url = new URL(String.format("https://api.coingecko.com/api/v3/simple/price?ids=%s&vs_currencies=usd", c.getCoinGeckoId()));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.connect();
                int responseCode = conn.getResponseCode();
                if (responseCode == 200) {
                    Scanner scanner = new Scanner(url.openStream());
                    StringBuilder response = new StringBuilder();
                    while (scanner.hasNext()) {
                        response.append(scanner.nextLine());
                    }
                    JSONObject responseObj = new JSONObject(String.valueOf(response));
                    JSONObject coinObj = (JSONObject) responseObj.get(c.getCoinGeckoId());
                    float price = Float.parseFloat(String.valueOf(coinObj.get("usd")));

                    String valueString = String.format("%,f", diff * price);
                    if (valueString.length() > 12)
                        valueString = valueString.substring(0, 12).replaceAll(",", " ");

                    table.addCell(cell(valueString, font));
                } else {
                    table.addCell(cell("NULL", font));
                }
            } else {
                table.addCell(cell("NULL", font));
            }
        }
    }

    public static <T> Predicate<T> distinctByKey(
            Function<? super T, ?> keyExtractor) {

        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
