package com.example.crypto_diary.repositories;

import com.example.crypto_diary.models.ERole;
import com.example.crypto_diary.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByName(ERole name);
}
