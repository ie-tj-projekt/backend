package com.example.crypto_diary.repositories;

import com.example.crypto_diary.models.Coin;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CoinRepository extends MongoRepository<Coin, String> {
    Optional<Coin> findBySymbol(String symbol);
}
