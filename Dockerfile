FROM linuxmintd/mint20-amd64

RUN apt update && apt install openjdk-11-jdk openjdk-11-jre -y

WORKDIR /app

CMD ["./mvnw", "spring-boot:run"]
